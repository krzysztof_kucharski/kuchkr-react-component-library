export const lightTheme = {
    width: "480px",
    height: "50px",
    border: 'none',
    borderRadius: '8px',
    textColor: '#2b2b2b',
    disabledTextColor: '#959595',
    backgroundColor: '#cecece',
    disabledBackgroundColor: '#dcdcdc',
    hoverBackgroundColor: '#c9c9c9',
    iconColor: "#2b2b2b",
    disabledIconColor: "#959595",
    fontSize: "1em",

    listStyle: {
        background: "#ffffff",
        listItemStyle: {
            textColor: "#2d2d2d",
            textColorHover: "#2d2d2d",
            disabledTextColor: "#9c9c9c",
            background: "#ffffff",
            disabledBackground: "#e5e5e5",
            backgroundHover: "#cecece",
            paddingLeft: "15px",
            paddingRight: "15px"
        }
    }
}

export const darkTheme = {
    width: "480px",
    height: "50px",
    border: 'none',
    borderRadius: '8px',
    textColor: '#959595',
    disabledTextColor: '#616161',
    backgroundColor: '#1c1c1c',
    disabledBackgroundColor: 'rgba(49,49,49,0.75)',
    hoverBackgroundColor: '#212121',
    iconColor: "#959595",
    disabledIconColor: "#4c4c4c",
    fontSize: "1em",

    listStyle: {
        background: "#313131",
        listItemStyle: {
            textColor: "#868686",
            disabledTextColor: "#525252",
            textColorHover: "#868686",
            background: "#363636",
            disabledBackground: "#2b2b2b",
            backgroundHover: "#424242",
            paddingLeft: "15px",
            paddingRight: "15px"
        }
    }
}