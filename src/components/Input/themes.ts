export const lightTheme = {
    backgroundColor: "rgba(255,255,255,0)",
    textColor: "#474747",
    placeholderTextColor: "#bababa",
    border: "1px solid " + "#afafaf",
    borderFocus: "1px solid " + "#666666",
    borderRadius: "0",
    height: "30px",
    width: "280px",
    padding: "0px",

    textTheme: {
        textColor: '#2f2f2f',
        fontSize: '0.9em',
        fontWeight: 'bold',
        margin: "0px 0px 0px 0px"
    }
}

export const darkTheme = {
    backgroundColor: "rgba(31,31,31,0)",
    textColor: "#cbcbcb",
    placeholderTextColor: "#4c4c4c",
    borderFocus: "1px solid " + "#c1c1c1",
    border: "1px solid " + "#666666",
    borderRadius: "0",
    height: "30px",
    width: "280px",
    padding: "0px",

    textTheme: {
        textColor: '#a0a0a0',
        fontSize: '0.9em',
        fontWeight: 'bold',
        margin: "0px 0px 0px 0px"
    }
}