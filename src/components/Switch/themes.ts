export const lightTheme = {
    trackBackgroundOff: "#c6c6c6",
    trackBackgroundOn: "#c6c6c6",
    knobBackground: "#eeeeee",
    knobBackgroundOn: "#0090ff"
}

export const darkTheme = {
    trackBackgroundOff: "#1f1f1f",
    trackBackgroundOn: "#1f1f1f",
    knobBackground: "#3d3d3d",
    knobBackgroundOn: "#999999"
}