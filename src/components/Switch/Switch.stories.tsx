import { Switch as SwitchComponent } from './Switch';
import { ComponentStory, generateStoryOptions } from "../../util/BaseComponentStory";

export default generateStoryOptions(SwitchComponent);

export const Switch = ComponentStory(SwitchComponent)