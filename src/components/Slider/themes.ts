export const lightTheme = {
    width: "400px",
    trackHeight: 2,
    railHeight: 2,
    fillProgressColor: '#337dff',
    thumbColor: '#337dff',
    thumbSize: 28,
    thumbMargin: {
        marginTop: -14,
        marginRight: 0,
        marginBottom: 0,
        marginLeft: -14
    },
    valueLabelBackground: '#337dff',
    valueLabelFontColor: '#ffffff',
    markLabelFontColor: '#adadad',
    markLabelActiveFontColor: '#337dff',
    markLabelActiveFontWeight: 700,
    markColor: '#adadad',
    markActiveColor: '#337dff',
    railColor: '#adadad',
    markSize: 8,
    markActiveSize: 8,
    markMargin: {
        marginTop: -3,
        marginRight: 0,
        marginBottom: 0,
        marginLeft: -2
    },
    markActiveMargin: {
        marginTop: -3,
        marginRight: 0,
        marginBottom: 0,
        marginLeft: -2
    }
}

export const darkTheme = {
    width: "400px",
    trackHeight: 2,
    railHeight: 2,
    fillProgressColor: '#337dff',
    thumbColor: '#337dff',
    thumbSize: 28,
    thumbMargin: {
        marginTop: -14,
        marginRight: 0,
        marginBottom: 0,
        marginLeft: -14
    },
    valueLabelBackground: '#337dff',
    valueLabelFontColor: '#ffffff',
    markLabelFontColor: '#adadad',
    markLabelActiveFontColor: '#337dff',
    markLabelActiveFontWeight: 700,
    markColor: '#adadad',
    markActiveColor: '#337dff',
    railColor: '#adadad',
    markSize: 8,
    markActiveSize: 8,
    markMargin: {
        marginTop: -3,
        marginRight: 0,
        marginBottom: 0,
        marginLeft: -2
    },
    markActiveMargin: {
        marginTop: -3,
        marginRight: 0,
        marginBottom: 0,
        marginLeft: -2
    }
}