import styled from "styled-components";
import { styledButtonDefaultProps, StyledButtonProps } from "./Button.types";

export const StyledButton = styled.button<StyledButtonProps>`
  width: ${props => props.theme.width ? props.theme.width : "150px"};
  height: ${props => props.theme.height ? props.theme.height : "40px"};
  border: ${props => props.theme.border};
  border-radius: ${props => props.theme.borderRadius};
  background: ${props => props.disabled ? props.theme.disabledBackground : props.theme.background};
  overflow: hidden;
  margin: ${props => props.theme.margin ? props.theme.margin : "0 0 0 0"};
  font-family: inherit;
  &:hover {
    cursor: ${props => {
      if (props.disabled) {
        return "unset";
      }
      return "pointer";
    }};

    background: ${props => props.disabled ? props.theme.disabledBackground : props.theme.hoverBackground};
  }
`

StyledButton.defaultProps = styledButtonDefaultProps;