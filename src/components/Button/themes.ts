export const lightTheme = {
    width: "150px",
    height: "40px",
    background: "#d2d2d2",
    disabledBackground: "rgba(210,210,210,0.37)",
    hoverBackground: "#c6c6c6",
    border: "none",
    borderRadius: "3px",

    text: {
        textColor: "#4e4e4e",
        disabledTextColor: "rgba(78,78,78,0.2)",
    }
}

export const darkTheme = {
    width: "150px",
    height: "40px",
    background: "#323232",
    disabledBackground: "rgba(47,47,47,0.43)",
    hoverBackground: "#393939",
    border: "none",
    borderRadius: "3px",

    text: {
        textColor: "#e5e5e5",
        disabledTextColor: "rgba(255,255,255,0.20)"
    }
}