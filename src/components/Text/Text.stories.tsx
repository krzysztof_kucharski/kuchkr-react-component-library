import { Text as TextComponent } from './Text';
import { ComponentStory, generateStoryOptions } from "../../util/BaseComponentStory";

export default generateStoryOptions(TextComponent);

export const Text = ComponentStory(TextComponent)