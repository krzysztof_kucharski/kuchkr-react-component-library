export const lightTheme = {
    textColor: "#313131",
    disabledTextColor: "#666666",
    fontSize: "20px",
    margin: "0px 0px 0px 0px",
    hoverColor: 'red'
}

export const darkTheme = {
    textColor: "#d0d0d0",
    disabledTextColor: "#9e9e9e",
    fontSize: "20px",
    margin: "0px 0px 0px 0px"
}