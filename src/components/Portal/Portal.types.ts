import React from "react";

export interface PortalProps {
    onClickOutside: Function,
    children: React.ReactNode
}